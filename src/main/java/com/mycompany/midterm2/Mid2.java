/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.midterm2;

import java.util.Scanner;

/**
 *
 * @author Tuf Gaming
 */
public class Mid2 {
   static void F(int s, int[] A) {
        for (int i = 0; i < s / 2; i++) {
            int j = A[i];
            A[i] = A[s - i - 1];
            A[s - i - 1] = j;

        }
        for (int i = 0; i < s; i++) {
            System.out.print(" " + A[i]);
        }}
    public static void main(String[] args) {
        Scanner kp = new Scanner(System.in);
        int s = kp.nextInt();
        int [] A = new int[s];

        for (int i = 0; i < s; i++) {
            A[i] = kp.nextInt();

        }
        F(s, A);

    }

    
    
}
